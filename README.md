# Spring Framework SchedulerApiServer

Job scheduling for multi node services.

## Features
Basically this package allows you to trigger scheduled job thru api call.

| Basic schema |
| ------ |
|![schema](/uploads/cb58820126f2e17f7f4da6a7ff970b0a/schema.png)|


## Installing
Add this dependency <br><br>
[![Maven Central](https://img.shields.io/maven-central/v/com.gitlab.katsella/schedulerapiserver.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.gitlab.katsella%22%20AND%20a:%22schedulerapiserver%22)
```
<dependency>
    <groupId>com.gitlab.katsella</groupId>
    <artifactId>schedulerapiserver</artifactId>
    <version>1.0.1-RC3</version>
</dependency>

```

## Usage
This package needs DataSource. Make sure you implement database connection to your project.<br>
I've developed and tested with postgresql. <br>

### Example postgresql database connection

```java
<dependency>
    <groupId>org.postgresql</groupId>
    <artifactId>postgresql</artifactId>
    <scope>runtime</scope>
</dependency>
```

```java
spring.datasource.url=jdbc:postgresql://localhost:5432/baseprojectdb
spring.datasource.username=postgres
spring.datasource.password=postgres
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
spring.jpa.hibernate.ddl-auto=update
```

### SchedulerApiServer configuration
```java
@Component
public class SchedulerApiConfig extends SchedulerApiConfiguration {
    @Override
    public void schedulerApiConfigure(SchedulerApi schedulerApi) {
        schedulerApi.addService("Test", "http://localhost:9711/api/v1/scheduler")  // Service name and endpoint
                .triggerFixedDelay("identityName", "my_job_name", 10_000, 1_000); // uniqueName, Scheduled method name, delay, timeout
    }
}
```

### If you need to set Authentication header
```java
public class MyRequestProcessor extends RestTemplateRequestProcessor {
    public MyRequestProcessor() {
        super(); // you can pass RestTemplate as parameter if needed
    }

    @Override
    public <T> T post(RequestModel requestModel, Class<T> tClass) {
        Map<String, String> headers = requestModel.getHeaders();
        if (Objects.isNull(headers))
            headers = new HashMap<>();
        headers.put("Authentication", "Bearer token=");
        return super.post(requestModel, tClass);
    }
}

@Component
public class RequestProcessorConfig {
    @Bean
    public RequestProcessor requestProcessor() {
        return new MyRequestProcessor();
    }
}
```
<br>
For SchedulerApiClient check out this ->  https://gitlab.com/katsella/SchedulerApiClient
