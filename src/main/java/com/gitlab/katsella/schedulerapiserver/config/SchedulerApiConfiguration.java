package com.gitlab.katsella.schedulerapiserver.config;

import com.gitlab.katsella.schedulerapiserver.SchedulerApiComponents;
import com.gitlab.katsella.schedulerapiserver.factory.SchedulerApiBean;
import com.gitlab.katsella.schedulerapiserver.processor.Request.RequestProcessor;
import com.gitlab.katsella.schedulerapiserver.processor.Request.impl.RestTemplateRequestProcessor;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.SchedulerApi;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.impl.SchedulerApiProcessor;
import com.gitlab.katsella.schedulerapiserver.service.IJobStatusService;
import com.gitlab.katsella.schedulerapiserver.service.Impl.JobStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.DefaultManagedTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.logging.Logger;

@Component
@DependsOn("SchedulerApiBean")
@ComponentScan(basePackageClasses = SchedulerApiComponents.class)
public abstract class SchedulerApiConfiguration {

    private static final Logger LOGGER = Logger.getLogger(SchedulerApiConfiguration.class.getName());

    public abstract void schedulerApiConfigure(SchedulerApi schedulerApi);

    @Autowired
    private SchedulerApiBean schedulerApiBean;

    @PostConstruct
    public void init() {
        IJobStatusService jobStatusService = new JobStatusService(schedulerApiBean.getJobStatusRepository(), schedulerApiBean.getSchedulerApiEnvironments());
        TaskScheduler taskScheduler = new DefaultManagedTaskScheduler();
        SchedulerApiProcessor schedulerApiProcessor = new SchedulerApiProcessor(jobStatusService, schedulerApiBean.getRequestProcessor(), taskScheduler);
        schedulerApiConfigure(schedulerApiProcessor);
        schedulerApiBean.getJobStatusController().registerEndpoint(schedulerApiBean.getSchedulerApiEnvironments().jobDoneEndpoint, jobStatusService);
        LOGGER.info("SchedulerApi initialized successfully");
    }
}
