package com.gitlab.katsella.schedulerapiserver.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("scheduler-api")
public class SchedulerApiEnvironments {
    String jobDoneEndpoint = "/api/v1/jobStatus/done";
    long dbRecordDay = 3;

    public String getJobDoneEndpoint() {
        return jobDoneEndpoint;
    }

    public long getDbRecordDay() {
        return dbRecordDay;
    }
}
