package com.gitlab.katsella.schedulerapiserver.controller;

import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model.SchedulerApiBody;
import com.gitlab.katsella.schedulerapiserver.service.IJobStatusService;
import com.gitlab.katsella.schedulerapiserver.util.VersionUtils;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.SpringVersion;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.pattern.PathPatternParser;

import java.util.logging.Logger;

@RestController
public class JobStatusController {

    private static final Logger LOGGER = Logger.getLogger(JobStatusController.class.getName());

    private IJobStatusService jobStatusService;

    private final RequestMappingHandlerMapping requestMappingHandlerMapping;

    public JobStatusController(RequestMappingHandlerMapping requestMappingHandlerMapping) {
        this.requestMappingHandlerMapping = requestMappingHandlerMapping;
    }

    public void registerEndpoint(String endpoint, IJobStatusService jobStatusService) {
        this.jobStatusService = jobStatusService;

        var mappingInfoBuilder = RequestMappingInfo
                .paths(endpoint)
                .methods(RequestMethod.POST);

        boolean isVersionBefore = VersionUtils.isVersionBefore(SpringVersion.getVersion(), "5.3.13");
        if (!isVersionBefore) {
            RequestMappingInfo.BuilderConfiguration options = new RequestMappingInfo.BuilderConfiguration();
            options.setPatternParser(new PathPatternParser());
            mappingInfoBuilder.options(options);
        }

        RequestMappingInfo mappingInfo = mappingInfoBuilder.build();

        try {
            requestMappingHandlerMapping.registerMapping(mappingInfo, this, this.getClass().getDeclaredMethod("finished", SchedulerApiBody.class));
            LOGGER.info("SchedulerApi job done endpoint: " + endpoint);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Probably bug. 'finished(SheduelerApiBody)' method not found");
        }
    }

    //    @PostMapping("/finished")
    public void finished(
            @RequestBody SchedulerApiBody schedulerApiBody) {
        jobStatusService.jobCompleted(schedulerApiBody);
    }

}
