package com.gitlab.katsella.schedulerapiserver.core.exception;

public class JobStatusNotFound extends RuntimeException {
    public JobStatusNotFound(String message) {
        super(message);
    }
}
