package com.gitlab.katsella.schedulerapiserver.entity;

import com.gitlab.katsella.schedulerapiserver.entity.base.BaseEntity;
import com.sun.istack.NotNull;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "job_status", uniqueConstraints = @UniqueConstraint(
        columnNames = {"service_name", "job_name", "finished_at"},
        name = "unq_job_status"
))
@Where(clause = "deleted_at is null")
public class JobStatus extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "job_status_seq")
    @SequenceGenerator(name = "job_status_seq", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "service_name", nullable = false)
    private String serviceName;

    @NotNull
    @Column(name = "job_name", nullable = false)
    private String jobName;

    // set when job succesfully completed
    @Column(name = "completed_at")
    private LocalDateTime completedAt;

    // set when job done, (succesfull or unsuccessfull)
    @Column(name = "finished_at")
    private LocalDateTime finishedAt;

    public JobStatus(Long id, String serviceName, String jobName, LocalDateTime completedAt, LocalDateTime finishedAt) {
        this.id = id;
        this.serviceName = serviceName;
        this.jobName = jobName;
        this.completedAt = completedAt;
        this.finishedAt = finishedAt;
    }

    public JobStatus() {

    }

    public JobStatus(String serviceName, String jobName) {
        this.serviceName = serviceName;
        this.jobName = jobName;
    }

    @PreUpdate
    void preUpdate() {
        if (Objects.nonNull(completedAt))
            finishedAt = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getJobName() {
        return jobName;
    }

    public LocalDateTime getCompletedAt() {
        return completedAt;
    }

    public LocalDateTime getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(LocalDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public void setCompletedAt(LocalDateTime completedAt) {
        this.completedAt = completedAt;
    }
}
