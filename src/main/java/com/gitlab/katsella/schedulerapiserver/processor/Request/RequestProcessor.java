package com.gitlab.katsella.schedulerapiserver.processor.Request;

import com.gitlab.katsella.schedulerapiserver.processor.Request.model.RequestModel;

public interface RequestProcessor {

    <T> T post(RequestModel requestModel, Class<T> tClass);

}
