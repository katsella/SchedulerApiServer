package com.gitlab.katsella.schedulerapiserver.processor.Request.model;

import java.util.Map;

public class RequestModel<T> {
    String url;
    Map<String, String> headers;
    T body;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public String getUrl() {
        return url;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public T getBody() {
        return body;
    }
}
