package com.gitlab.katsella.schedulerapiserver.processor.Scheduler;

import com.gitlab.katsella.schedulerapiserver.core.exception.ServiceExistException;
import com.gitlab.katsella.schedulerapiserver.core.exception.ServiceNotFoundException;

public interface SchedulerApi {
    /**
     * Add new service configuration.
     *
     * @param serviceName Unique name for service, you can retrive ServiceScheduler with serviceName
     * @param endpointUrl ex. http://localhost:8080/api/v1/scheduler
     * @return
     * @throws ServiceExistException
     */
    ServiceScheduler addService(String serviceName, String endpointUrl) throws ServiceExistException;

    /**
     * Get ServiceScheduler with serviceName
     *
     * @param serviceName Unique name for service
     * @return
     * @throws ServiceNotFoundException
     */
    ServiceScheduler getService(String serviceName) throws ServiceNotFoundException;
}
