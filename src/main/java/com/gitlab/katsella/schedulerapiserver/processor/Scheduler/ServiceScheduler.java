package com.gitlab.katsella.schedulerapiserver.processor.Scheduler;

import com.gitlab.katsella.schedulerapiserver.core.exception.ScheduledJobNotFound;

public interface ServiceScheduler {
    /**
     * Add fixed rate scheduler job.
     *
     * @param uniqueName   name for scheduled job, you can control scheduler job with this name
     * @param jobName      SchedulerApiClient defined job name, defaulth job name format: {ClassName}.{MethodName}
     * @param initialDelay ms initial delay
     * @param period       ms period
     * @param timeout      ms timeout, after job triggered, job won't be triggerred again until timeout
     * @return
     */
    ServiceScheduler triggerFixedRate(String uniqueName, String jobName, long initialDelay, long period, long timeout);

    /**
     * Add fixed rate scheduler job.
     *
     * @param uniqueName name for scheduled job, you can control scheduler job with this name
     * @param jobName    SchedulerApiClient defined job name, defaulth job name format: {ClassName}.{MethodName}
     * @param period     ms period
     * @param timeout    ms timeout, after job triggered, job won't be triggerred again until timeout
     * @return
     */
    ServiceScheduler triggerFixedRate(String uniqueName, String jobName, long period, long timeout);

    /**
     * Add fixed delay scheduler job.
     *
     * @param uniqueName   unique name for scheduled job, you can control scheduler job with this name
     * @param jobName      SchedulerApiClient defined job name, defaulth job name format: {ClassName}.{MethodName}
     * @param initialDelay ms initial delay
     * @param period       ms period
     * @param timeout      ms timeout, after job triggered, job won't be triggerred again until timeout
     * @return
     */
    ServiceScheduler triggerFixedDelay(String uniqueName, String jobName, long initialDelay, long period, long timeout);

    /**
     * Add fixed delay scheduler job.
     *
     * @param uniqueName unique name for scheduled job, you can control scheduler job with this name
     * @param jobName    SchedulerApiClient defined job name, defaulth job name format: {ClassName}.{MethodName}
     * @param period     ms period
     * @param timeout    ms timeout, after job triggered, job won't be triggerred again until timeout
     * @return
     */
    ServiceScheduler triggerFixedDelay(String uniqueName, String jobName, long period, long timeout);

    /**
     * Add cron scheduler job.
     *
     * @param uniqueName     unique name for scheduled job, you can control scheduler job with this name
     * @param jobName        SchedulerApiClient defined job name, defaulth job name format: {ClassName}.{MethodName}
     * @param cronExpression cron expression "* * * * * *"
     * @param timeout        ms timeout, after job triggered, job won't be triggerred again until timeout
     * @return
     */
    ServiceScheduler triggerCron(String uniqueName, String jobName, String cronExpression, long timeout);

    /**
     * Trigger defined scheduler job.
     *
     * @param uniqueName unique name for scheduled job
     * @throws ScheduledJobNotFound
     */
    void run(String uniqueName) throws ScheduledJobNotFound;

    void cancel(String uniqueName) throws ScheduledJobNotFound;
}
