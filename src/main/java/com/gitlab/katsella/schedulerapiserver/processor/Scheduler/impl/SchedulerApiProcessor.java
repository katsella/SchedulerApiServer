package com.gitlab.katsella.schedulerapiserver.processor.Scheduler.impl;

import com.gitlab.katsella.schedulerapiserver.core.exception.ServiceExistException;
import com.gitlab.katsella.schedulerapiserver.core.exception.ServiceNotFoundException;
import com.gitlab.katsella.schedulerapiserver.processor.Request.RequestProcessor;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.SchedulerApi;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.ServiceScheduler;
import com.gitlab.katsella.schedulerapiserver.service.IJobStatusService;
import org.springframework.scheduling.TaskScheduler;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SchedulerApiProcessor implements SchedulerApi {
    private Map<String, ServiceScheduler> serviceMap = new HashMap<>();

    private RequestProcessor requestProcessor;

    private TaskScheduler taskScheduler;

    private IJobStatusService jobStatusService;

    public SchedulerApiProcessor(IJobStatusService jobStatusService, RequestProcessor requestProcessor, TaskScheduler taskScheduler) {
        this.requestProcessor = requestProcessor;
        this.taskScheduler = taskScheduler;
        this.jobStatusService = jobStatusService;
    }

    public ServiceScheduler addService(String serviceName, String endpointUrl) throws ServiceExistException {
        if (serviceMap.containsKey(serviceName))
            throw new ServiceExistException("Service name exist. serviceName: " + serviceName);
        ServiceScheduler scheduler = new ServiceSchedulerProcessor(endpointUrl, serviceName, taskScheduler, requestProcessor, jobStatusService);
        serviceMap.put(serviceName, scheduler);
        return scheduler;
    }

    public ServiceScheduler getService(String serviceName) throws ServiceNotFoundException {
        ServiceScheduler serviceScheduler = serviceMap.get(serviceName);
        if (Objects.isNull(serviceScheduler))
            throw new ServiceNotFoundException("Requested service name not fonud. serviceName: " + serviceName);
        return serviceScheduler;
    }
}
