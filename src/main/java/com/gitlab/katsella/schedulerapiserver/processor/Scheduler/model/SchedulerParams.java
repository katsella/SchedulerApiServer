package com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model;

import java.time.LocalDateTime;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SchedulerParams {
    private String uniqueName;
    private String jobName;
    private Lock lock;
    private final boolean lockTask;
    private LocalDateTime lastTriggeredTime;
    private Long timeout;

    public SchedulerParams(String uniqueName, String jobName, boolean lockTask, Long timeout) {
        this.uniqueName = uniqueName;
        this.jobName = jobName;
        this.lockTask = lockTask;
        this.timeout = timeout;
        if (lockTask)
            lock = new ReentrantLock();
    }

    public void setLastTriggeredTime(LocalDateTime lastTriggeredTime) {
        this.lastTriggeredTime = lastTriggeredTime;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public String getJobName() {
        return jobName;
    }

    public Lock getLock() {
        return lock;
    }

    public boolean isLockTask() {
        return lockTask;
    }

    public LocalDateTime getLastTriggeredTime() {
        return lastTriggeredTime;
    }

    public Long getTimeout() {
        return timeout;
    }
}
