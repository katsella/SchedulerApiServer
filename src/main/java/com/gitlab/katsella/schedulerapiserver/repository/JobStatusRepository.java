package com.gitlab.katsella.schedulerapiserver.repository;


import com.gitlab.katsella.schedulerapiserver.entity.JobStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface JobStatusRepository extends JpaRepository<JobStatus, Long> {
    List<JobStatus> findByServiceNameAndJobNameAndFinishedAtIsNull(String serviceType, String jobName);

//    JobStatus save(JobStatus jobStatus);
//
//    List<JobStatus> saveAll(List<JobStatus> jobStatusList);
//
//    JobStatus getById(long id);

    long deleteByCreatedAtIsLessThan(LocalDateTime createdAt);

}
