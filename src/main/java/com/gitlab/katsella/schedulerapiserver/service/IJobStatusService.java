package com.gitlab.katsella.schedulerapiserver.service;

import com.gitlab.katsella.schedulerapiserver.entity.JobStatus;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model.SchedulerApiBody;

public interface IJobStatusService {
    JobStatus jobCreate(String serviceName, String jobName);

    boolean isJobExist(String serviceName, String jobName, long timeout);

    void jobCompleted(SchedulerApiBody schedulerApiBody);
}
