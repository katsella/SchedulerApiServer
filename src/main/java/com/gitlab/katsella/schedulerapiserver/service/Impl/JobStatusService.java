package com.gitlab.katsella.schedulerapiserver.service.Impl;

import com.gitlab.katsella.schedulerapiserver.config.SchedulerApiEnvironments;
import com.gitlab.katsella.schedulerapiserver.core.exception.JobStatusNotFound;
import com.gitlab.katsella.schedulerapiserver.entity.JobStatus;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model.SchedulerApiBody;
import com.gitlab.katsella.schedulerapiserver.repository.JobStatusRepository;
import com.gitlab.katsella.schedulerapiserver.service.IJobStatusService;
import com.gitlab.katsella.schedulerapiserver.util.MapperUtils;
import com.gitlab.katsella.schedulerapiserver.util.TimeUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JobStatusService implements IJobStatusService {

    private static final Logger LOGGER = Logger.getLogger(JobStatusService.class.getName());

    private final JobStatusRepository jobStatusRepository;

    private final SchedulerApiEnvironments schedulerApiEnvironments;

    private final Long NANOS_TO_MILLIS = 1_000_000l;

    public JobStatusService(JobStatusRepository jobStatusRepository, SchedulerApiEnvironments schedulerApiEnvironments) {
        this.jobStatusRepository = jobStatusRepository;
        this.schedulerApiEnvironments = schedulerApiEnvironments;
    }

    public JobStatus jobCreate(String serviceName, String jobName) {
        JobStatus jobStatus = new JobStatus(serviceName, jobName);

        return jobStatusRepository.save(jobStatus);
    }

    public boolean isJobExist(String serviceName, String jobName, long timeout) {
        List<JobStatus> jobList = jobStatusRepository.findByServiceNameAndJobNameAndFinishedAtIsNull(serviceName, jobName);

        if (CollectionUtils.isEmpty(jobList))
            return false;

        if (jobList.size() > 1) {
            List<JobStatus> dublicatedValue = jobList.subList(1, jobList.size() - 1);
            for (JobStatus jobStatus : dublicatedValue)
                jobStatus.setFinishedAt(LocalDateTime.now());
            jobStatusRepository.saveAll(dublicatedValue);
        }

        JobStatus statusEntity = jobList.get(0);
        if (statusEntity.getCreatedAt().plusNanos(timeout * NANOS_TO_MILLIS).isBefore(LocalDateTime.now())) {
            statusEntity.setFinishedAt(LocalDateTime.now());
            statusEntity = jobStatusRepository.save(statusEntity);
            LOGGER.log(Level.SEVERE, "isJobExist::error Job timeout, jobStatusEntity: " + MapperUtils.writeValueAsStringDateFormattedWithoutException(statusEntity));
            return false;
        } else
            return true;
    }

    public void jobCompleted(SchedulerApiBody schedulerApiBody) {
        JobStatus jobStatus = jobStatusRepository.getOne(schedulerApiBody.getJobId());
        if (Objects.isNull(jobStatus))
            throw new JobStatusNotFound("JobStatusEntity not fount by id. schedulerApiBody: " + MapperUtils.writeValueAsStringDateFormattedWithoutException(schedulerApiBody));

        if (Objects.nonNull(jobStatus.getCompletedAt()))
            return;

        jobStatus.setCompletedAt(LocalDateTime.now());
        jobStatus = jobStatusRepository.save(jobStatus);
        jobCompletedLog(jobStatus);
    }

    private void jobCompletedLog(JobStatus jobStatus) {
        if (Objects.isNull(jobStatus.getCreatedAt()) || Objects.isNull(jobStatus.getCompletedAt())) {
            LOGGER.log(Level.SEVERE, "jobCompletedLog::error Something wrong, date is null jobStatusEntity: " + MapperUtils.writeValueAsStringDateFormattedWithoutException(jobStatus));
            return;
        }
        Long deltaTime = TimeUtils.deltaTimeMillis(jobStatus.getCreatedAt(), jobStatus.getCompletedAt());
        LOGGER.info(MessageFormat.format("[{0}]({1}), jobId: {2}, timeElapsed: {3}", jobStatus.getServiceName(), jobStatus.getJobName(), jobStatus.getId(), deltaTime));
    }

    @Transactional
    @Scheduled(cron = "${0 0 0 * * *}")
    public void scheduledDelete() {
        LocalDateTime date = LocalDateTime.now().minusDays(schedulerApiEnvironments.getDbRecordDay());
        long deletedCount = jobStatusRepository.deleteByCreatedAtIsLessThan(date);
        LOGGER.info(MessageFormat.format("Total {0} jobStatus, older than {1} deleted", deletedCount, date));
    }
}
