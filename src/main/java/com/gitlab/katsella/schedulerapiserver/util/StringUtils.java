package com.gitlab.katsella.schedulerapiserver.util;

public class StringUtils {
    public static boolean isBlank(String str) {
        if (str == null)
            return true;

        if (str.equals(""))
            return true;

        if (str.replace(" ", "") == "")
            return true;
        return false;
    }
}
